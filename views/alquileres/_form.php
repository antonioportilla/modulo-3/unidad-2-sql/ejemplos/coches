<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Alquileres */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="alquileres-form">

    <?php $form = ActiveForm::begin(); ?>

    <!-- cuadro de lista usuarios   -->
    <?= $form->field($model, 'usuario')->dropDownList(
            $model->Usuarios
            ) ?> 
    <!--  fin de cuadro de lista   usuarios  -->
 
    <!-- cuadro de lista coches   -->
    <?= $form->field($model, 'coche')->dropDownList(
            $model->Coches
            ) ?>
    <!--  fin de cuadro de lista   coches  -->

    
    
    <?php
    /* opcion uno del Datapickerd 
    echo '<label class="control-label">'.$model->getAttributeLabel("fecha").'</label>'; //opcion de sacar la eqiqueta de un atributo
    echo DatePicker::widget([
        'model' => $model, 
        'attribute' => 'fecha',
        'options' => ['placeholder' => 'Introduce la fecha de Alquiler'],
        'pluginOptions' => [
            'todayHighlight' => true,
            'todayBtn' => true,
            'autoclose'=>true,
            'format'=>'yyyy/mm/dd',
        ]
    ]);  
     */
    
    echo $form->field($model,'fecha')->widget(DatePicker::className(),[ //opcion 2 de sacar las etiquetas de los atributos
        'options' => ['placeholder' => 'Introduce la fecha de Alquiler'],
        'pluginOptions' => [
            'todayHighlight' => true,
            'todayBtn' => true,
            'autoclose'=>true,
            /* 'format'=>'yyyy/mm/dd',      opcion de guardar con fecha formato original*/ 
            'format'=>'dd/mm/yyyy', 
        ]
    ]);  
    
    
    
    
    ?>
    

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
