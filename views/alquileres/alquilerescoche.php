<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alquileres';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alquileres-index">

    
        <div class="jumbotron">
            <h1> Estos son los alquileres de 
            
         <?= $dataProvider->models[0]->coche  ?>
                
            </h1>
        </div>
        
    
    


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigoAlquiler',
            'coche',
            'fecha',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <p>
        <?= Html::a('Atras', ['/alquileres/index'], ['class' => 'btn btn-success']) ?>
    </p>


