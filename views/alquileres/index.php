<?php

use yii\helpers\Html;
use yii\grid\GridView;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alquileres';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alquileres-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Alquileres', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigoAlquiler',
            'usuario',
            'usuario0.nombre', // sacar un dato de otra tabla sin sacar la consulta
            [
                'label'=>'Nombre del usuario',
                'format'=>'raw',
                'content'=>function($ml){
                    return Html::a(
                            'ver aquileres de  '.
                            $ml->usuario0->nombre,
                            [
                                'alquileres/alquileresusuarios',
                                'id'=>$ml->usuario
                            ], 
                            [
                                'class'=>'btn btn-primary'
                            ]
                            );
                }
            ],
            'coche',
             [
                'label'=>'Codigo del coche ',
                'format'=>'raw',
                'content'=>function($ml){
                    return Html::a(
                            'ver coches '.
                            $ml->coche,
                            [
                                'alquileres/alquilerescoche',
                                'id'=>$ml->coche
                            ], 
                            [
                                'class'=>'btn btn-primary'
                            ]
                            );
                }
             ]       
                   
                    
           ,  // sacar un dato de otra tabla sin sacar la consulta
                      [
                'label'=>'Codigo del coche ',
                'format'=>'raw',
                'content'=>function($ml){
                    return Html::a(
                            'ver coches '.
                            $ml->coche,
                            [
                                'alquileres/alquilerescoche',
                                'id'=>$ml->coche
                            ], 
                            [
                                'class'=>'btn btn-primary'
                            ]
                            );
                }
             ]       
            ,  'coche0.marca',
                     // sacar un dato de otra tabla sin sacar la consulta
         
            
                     [
                'label'=>'Codigo del coche ',
                'format'=>'raw',
                'content'=>function($model){
                    return Html::a(
                            'ver coches '.
                            $model->coche0["marca"],
                            [
                                'alquileres/alquilerescoche',
                                'id'=>$model->coche0["marca"]
                            ], 
                            [
                                'class'=>'btn btn-primary'
                            ]
                            );
                }
             ]     
                               
            ,

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
