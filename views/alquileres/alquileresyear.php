<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Alquileres del año ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alquileres-index">

    
        <div class="jumbotron">
            <h1> Estos son los alquileres de 
            
         <?= $dataProvider->models[0]->campoyear  ?>
             
            </h1>
        </div>
        
    
    


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigoAlquiler',
            'coche',
            'fecha',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <p>
        <?= Html::a('Atras', ['/alquileres/index'], ['class' => 'btn btn-success']) ?>
    </p>


