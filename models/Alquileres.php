<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alquileres".
 *
 * @property int $codigoAlquiler
 * @property int $usuario
 * @property int $coche
 * @property string $fecha
 *
 * @property Coches $coche0
 * @property Usuarios $usuario0
 */
class Alquileres extends \yii\db\ActiveRecord
{
    public $campoyear;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alquileres';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['usuario', 'coche'], 'integer'],
            [['fecha'], 'safe'],
            [['usuario', 'coche', 'fecha'], 'unique', 'targetAttribute' => ['usuario', 'coche', 'fecha']],
            [['coche'], 'exist', 'skipOnError' => true, 'targetClass' => Coches::className(), 'targetAttribute' => ['coche' => 'codigoCoche']],
            [['usuario'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['usuario' => 'codigoUsuario']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoAlquiler' => 'Codigo Alquiler',
            'usuario' => 'Usuario',
            'coche' => 'Coche',
            'fecha' => 'Fecha Alquiler',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoche0()
    {
        return $this->hasOne(Coches::className(), ['codigoCoche' => 'coche']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsuario0()
    {
        return $this->hasOne(Usuarios::className(), ['codigoUsuario' => 'usuario']);
    }
    
     /* metodo nuevo, el listado de fecha sale en castellano */ 
    public function afterFind(){
        parent::afterFind();
        // tendria el año de un registro de alquileres automaticamente de forma dinamica
        // $this->campoyear=Yii::$app->formatter->asDate($this->fecha, 'php:Y');
        $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:d/m/Y');
    }
    
    /* antes de grabar, el listado de fecha guarda en castellano*/
    public function beforeSave($insert){
        parent::beforeSave($insert);
        /* $this->fecha=Yii::$app->formatter->asDate($this->fecha, 'php:Y-m-d');  fecha para guardr con formato original*/ 
        $this->fecha= \DateTime::createFromFormat("d/m/Y", $this->fecha)
                -> format("Y/m/d");
        
        return(true);
    }
    
    /* funcion ejemplo   */
    public static function listar(){
        return self::find()->all();
    }
    
    /* funcion combo   */
    
    /* desplegble */
 
    public function getCoches(){
        $coches=\app\models\Coches::find()->all();
        $coches=\yii\helpers\ArrayHelper::map($coches, "codigoCoche", "color","marca");
        return $coches;
    }
    
    
    public function getUsuarios(){
        $usuarios=\app\models\Usuarios::find()->all();
        $usuarios=\yii\helpers\ArrayHelper::map($usuarios, "codigoUsuario", "nombre");
        return $usuarios;
    }
    
    /*   Consulta de alquileres */
    public static function getAlquileresusuarios($id){
        return self::find()
                ->where("usuario=$id");
    }
    
    /*   Consulta de alquileres del año de la fecha */
    public static function getAlquileresyear($id){
        
        /*
         * select year(fecha) from alquileres where cofgoAlquiler=$id
         */
        
        $registro=self::find()
                ->select("YEAR(fecha) campoyear")
                ->where("codigoAlquiler=$id")
                ->one();
        
        $anno=$registro->campoyear;
        
        return self::find()
           ->select("*, YEAR(fecha) campoyear")
           ->where("year(fecha)=$anno");
        
    }
    
        public static function getAlquilerecoche($id){
        return self::find()
                 ->where("coche=$id");
    }
    
}
